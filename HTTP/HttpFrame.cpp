#include "HttpFrame.h"
#include "Debug.h"

#include <algorithm>  // transform std::all_of

/// Ethernet v2 max MTU 1500 bytes - IP header (20 bytes) - TCP header (20bytes)
OSEF::HttpFrame::HttpFrame()
    :NetBuffer(1460, 0) {}

bool OSEF::HttpFrame::pushHeader(const std::string& method, const std::string& uri, const HF_Container& hfields)
{
    const std::string sline = method + " " + uri + " " + HttpVersion;

    const bool ret = pushHeader(sline, hfields);
    return ret;
}

bool OSEF::HttpFrame::pushHeader(const std::string& sline, const HF_Container& hfields)
{
    bool ret  = false;

    if (pushStartLine(sline))
    {
        ret = true;

//        for (HF_Pair field : hfields)
//        {
//            ret &= pushHeaderField(field.first, field.second);
//        }
//
//        if (ret)
//        {
//            ret = pushEmptyLine();
//        }

        if ( std::all_of(hfields.begin(), hfields.end(), [this](const HF_Pair& field){return pushHeaderField(field.first, field.second);}) )
        {
            ret = pushEmptyLine();
        }
    }

    return ret;
}

bool OSEF::HttpFrame::pushStartLine(const std::string& sline)
{
    const bool ret = pushString(sline + "\r", '\n');
    return ret;
}

bool OSEF::HttpFrame::pushHeaderField(const std::string& name, const std::string& value)
{
    const bool ret = pushString(name + ": " + value + "\r", '\n');  // pushes string with end of string character
    return ret;
}

bool OSEF::HttpFrame::pushEmptyLine()
{
    const bool ret = pushString("\r", '\n');
    return ret;
}

bool OSEF::HttpFrame::extractHeaderUri(const std::string& method, std::string& uri, const std::string& version)
{
    bool ret = false;

    if (extractHeader())
    {
        const size_t lineSize = startLine.size();

        const std::string start = method + " ";
        const size_t startSize = start.size();

        const std::string end = " " + version;
        const size_t endSize = end.size();

        if (startLine.find(start) == 0)
        {
            size_t p = startLine.find(end);
            if (p == (lineSize-endSize))
            {
                uri = startLine.substr(startSize, (lineSize-endSize-startSize));
                ret = true;
            }
            else
            {
                DERR("start line version " << startLine << " different from expected " << version);
            }
        }
        else
        {
            DERR("start line method " << startLine << " different from expected " << method);
        }
    }

    return ret;
}

bool OSEF::HttpFrame::extractHeaderStartLine(const std::string& sline)
{
    bool ret = false;

    if (extractHeader())
    {
        if (startLine == sline)
        {
            ret = true;
        }
        else
        {
            DERR("start line " << startLine << " different from expected " << sline);
        }
    }

    return ret;
}

bool OSEF::HttpFrame::extractResponseHeader()
{
    const bool ret = extractHeaderStartLine(HttpStatusOK);
    return ret;
}

bool OSEF::HttpFrame::storeHeaderField(bool& emptyLine)
{
    bool ret = false;

    std::string name;
    std::string value;

    if (extractHeaderField(name, value, emptyLine))
    {
        if (not emptyLine)
        {
            if (headerFields.insert({name, value}).second)
            {
                ret = true;
            }
            else
            {
                DERR("error inserting " << name << ": " << value << " into header fields container");
            }
        }
    }

    return ret;
}

bool OSEF::HttpFrame::extractHeader()
{
    bool ret = false;

    if (extractStartLine())
    {
//        std::string name = "";
//        std::string value = "";
        bool emptyLine = false;

        do
        {
            ret = storeHeaderField(emptyLine);
//            ret = false;
//            if (extractHeaderField(name, value, emptyLine))
//            {
//                if (!emptyLine)
//                {
//                    if (headerFields.insert({name, value}).second)
//                    {
//                        ret = true;
//                    }
//                    else
//                    {
//                        DERR("error inserting " << name << ": " << value << " into header fields container");
//                    }
//                }
//            }
        }while (ret && (getRemainingSizeToExtract() > 0));

        if ((getRemainingSizeToExtract() == 0) && emptyLine)
        {
            ret = true;
        }
    }

    return ret;
}

bool OSEF::HttpFrame::extractStartLine()
{
    bool ret = false;

    if (extractString(startLine, HttpFieldMaxLength, '\n'))
    {
        if (startLine.back() == '\r')
        {
            if (startLine.erase(startLine.size()-1, 1) == startLine)
            {
                ret = true;
            }
        }
    }

    return ret;
}

bool OSEF::HttpFrame::extractHeaderFieldValue(std::string& value, const std::string& field, const size_t& separator)
{
    bool ret = false;

    if (field.size() >= (separator+2))
    {
        if (field.at(separator+1) == ' ')
        {
            value = field.substr(separator+2, field.size()-1);
            ret = true;
        }
        else
        {
            DERR("separator character : not followed by space");
        }
    }
    else
    {
        ret = true;
//                                _DOUT("empty field " << name << " value");
    }

    return ret;
}

bool OSEF::HttpFrame::extractHeaderField(std::string& name, std::string& value, bool& emptyLine)
{
    bool ret = false;

    name = "";
    value = "";
    emptyLine = false;

    std::string field;
    if (extractString(field, HttpFieldMaxLength, '\n'))
    {
        if (field.back() == '\r')
        {
            if (field.erase(field.size()-1, 1) == field)
            {
                if (not field.empty())
                {
                    size_t separator = field.find_first_of(':');
                    if (separator != std::string::npos)
                    {
                        if (separator > 0UL)  // check name is at least 1 character long
                        {
                            // extract field name
                            name = field.substr(0, separator);

                            // normalize field name to UPPER case
                            std::transform(name.begin(), name.end(), name.begin(), [](unsigned char c){return std::toupper(c);});

                            // extract field value
                            ret = extractHeaderFieldValue(value, field, separator);

//                            if (field.size() >= (separator+2))
//                            {
//                                if (field.at(separator+1) == ' ')
//                                {
//                                    value = field.substr(separator+2, field.size()-1);
//                                    ret = true;
//                                }
//                                else
//                                {
//                                    DERR("separator character : not followed by space");
//                                }
//                            }
//                            else
//                            {
//                                ret = true;
////                                _DOUT("empty field " << name << " value");
//                            }
                        }
                        else
                        {
                            DERR("empty field name");
                        }
                    }
                    else
                    {
                        DERR("no separator character : found");
                    }
                }
                else
                {
                    emptyLine = true;
                    ret = true;  // empty field marks end of frame
                }
            }
        }
        else
        {
            DERR("field \"" << field << "\" (" << field.size() << ") not ended by mandatory \\r character");
        }
    }

    return ret;
}

bool OSEF::HttpFrame::getField(const std::string& name, std::string& value) const
{
    bool ret = false;

    auto field = headerFields.find(name);
    if (field != headerFields.end())
    {
        value = field->second;
        ret = true;
    }
    else
    {
        DWARN("couldn't find header field " << name);
    }

    return ret;
}

bool OSEF::HttpFrame::isFieldEqual(const HF_Pair& field)
{
    const bool ret = isFieldEqual(field.first, field.second);
    return ret;
}

bool OSEF::HttpFrame::isFieldEqual(const std::string& name, const std::string& value)
{
    bool ret = false;

    HF_ConstIterator field = headerFields.find(name);
    if (field != headerFields.end())
    {
        if (field->second == value)
        {
            ret = true;
        }
        else
        {
            DERR("field " << name << ":" << field->second << " is not equal to " << value);
        }
    }

    return ret;
}

//    void OSEF::HttpFrame::encodeToBase64(const uint8_t* in, const size_t& insize, std::string& out, const size_t& outsize)
//    {
//        out.resize(outsize);
//
//        for (size_t i = 0; i < outsize; i++)
//        {
//            size_t j = 3*(i/4);
//            size_t j1 = j+1;
//            size_t j2 = j+2;
//            if (j < insize)
//            {
//                switch (i % 4)
//                {
//                    case 0:
//                        out[i] = Base64Alphabet[static_cast<size_t>((in[j]&0xfc)>>2)];
//                        break;
//                    case 1:
//                        if ((j+1) < insize)
//                        {
//                            out[i] = Base64Alphabet[static_cast<size_t>(((in[j]&0x03) << 4) + ((in[j1]&0xf0)>>4))];
//                        }
//                        else
//                        {
//                            out[i] = Base64Alphabet[static_cast<size_t>(((in[j]&0x03) << 4))];
//                        }
//                        break;
//                    case 2:
//                        if ((j+2) < insize)
//                        {
//                            out[i] = Base64Alphabet[static_cast<size_t>(((in[j1]&0x0f) << 2) + ((in[j2]&0xc0)>>6))];
//                        }
//                        else
//                        {
//                            if ((j+1) < insize)
//                            {
//                                out[i] = Base64Alphabet[static_cast<size_t>(((in[j1]&0x0f) << 2))];
//                            }
//                            else
//                            {
//                                out[i] = '=';
//                            }
//                        }
//                        break;
//                    case 3:
//                        if ((j+2) < insize)
//                        {
//                            out[i] = Base64Alphabet[static_cast<size_t>((in[j2]&0x3f))];
//                        }
//                        else
//                        {
//                            out[i] = '=';
//                        }
//                        break;
//                    default:
//                        break;
//                }
//            }
//        }
//    }
