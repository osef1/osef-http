#ifndef OSEFHTTPFRAME_H
#define OSEFHTTPFRAME_H

#include "NetBuffer.h"

#include <string>
#include <utility>  // pair
#include <unordered_map>

namespace OSEF
{
    const int8_t Base64Alphabet[64] =
    {
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '+', '/'
    };

    const size_t HttpFieldMaxLength = 512;  // arbitrary maximum length for a single field

    const std::string HttpVersion = "HTTP/1.1";

    const std::string HttpGet = "GET";

    const std::string HttpStatusOK = HttpVersion + " 200 OK";
    const std::string HttpBadRequest = HttpVersion + " 400 Bad Request";

    //  HTTP Header Fields
    typedef std::string HF_Name;
    typedef std::string HF_Value;
    typedef std::pair<HF_Name, HF_Value> HF_Pair;
    typedef std::unordered_map<HF_Name, HF_Value> HF_Container;
    typedef HF_Container::const_iterator HF_ConstIterator;

    // https://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html

    class HttpFrame : public OSEF::NetBuffer
    {
    public:
        HttpFrame();
        ~HttpFrame() override = default;

        bool pushHeader(const std::string& method, const std::string& uri, const HF_Container& hfields = {});
        bool pushHeader(const std::string& sline, const HF_Container& hfields = {});
        bool extractHeaderUri(const std::string& method, std::string& uri, const std::string& version = HttpVersion);
        bool extractHeaderStartLine(const std::string& sline);
        bool extractHeader();

        bool extractResponseHeader();

        bool getField(const std::string& name, std::string& value) const;
        bool isFieldEqual(const std::string& name, const std::string& value);
        bool isFieldEqual(const HF_Pair& field);

        std::string getStartLine() const {return startLine;}
        HF_Container getHeaderFields() const {return headerFields;}

        HttpFrame(const HttpFrame&) = delete;  // copy constructor
        HttpFrame& operator=(const HttpFrame&) = delete;  // copy assignment
        HttpFrame(HttpFrame&&) = delete;  // move constructor
        HttpFrame& operator=(HttpFrame&&) = delete;  // move assignment

    protected:
        bool pushStartLine(const std::string& line);
        bool pushHeaderField(const std::string& name, const std::string& value);
        bool pushEmptyLine();

        bool extractHeaderField(std::string& name, std::string& value, bool& emptyLine);
        bool extractHeaderFieldValue(std::string& value, const std::string& field, const size_t& separator);
        bool extractStartLine();

        bool storeHeaderField(bool& emptyLine);

//        void encodeToBase64(const uint8_t* in, const size_t& insize, std::string& out, const size_t& outsize);

    private:
        std::string startLine;
        HF_Container headerFields;
    };
}  // namespace OSEF

#endif /* OSEFHTTPFRAME_H */
